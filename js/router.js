define([
  'jquery',
  'underscore',
  'backbone',
  'session'
], function($, _, Backbone, Session){
  var AppRouter = Backbone.Router.extend({
    routes: {
      // API Editor
      "apis/:api/:category/*method": "apiEditor",
      "apis/:api/:category": "apiEditor",
      "apis/:api": "apiEditor",
      "apis": "apiEditor",
      
      // Public APIS
      "public": "public",
     
      // Registration
      "register": "register",
 
      // Authentication
      "logout": "logout",
      "login": "login",
      
      // Default
      "*actions": "default"
    },
    apiEditor: function(api, category, method){
      require(['views/api/main'], function(apiMain) {
        console.log(arguments);
        apiMain.render( api, category, method);
      });
    
    },
    public: function(){
      $("#content").html("Not implemented");
    },
    register: function(){
      
    },
    logout: function(){
     
    },
    default: function(actions){
      console.log('qwe');
      window.location = '#/apis';
      
    }
  });
  var initialize = function(){
    app_router = new AppRouter;
    Backbone.history.start();
  };
  return { 
    initialize: initialize
  };
});
