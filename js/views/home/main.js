define([
  'jquery',
  'underscore',
  'backbone',
  'session',
  'text!templates/home/main.html'
], function($, _, Backbone, Session, homeMainTemplate){
  var homeMainView = Backbone.View.extend({
    render: function(){
      $("#content").html(homeMainTemplate).hide();
      $("#content").fadeIn(200);

    },
    initialize: function(){
      this.loginHandler();
      this.registerHandler();
    },
    loginHandler: function(){
      $(".loginForm input[type=submit]").live("click", function(ev){
        var loginDetails = $(".loginForm").serializeObject($(".loginForm"));
        $(ev.currentTarget).addClass("loading");
        $(".loginerror").text("");
        Session.login(loginDetails, function(data){
	 if( data.auth ){
          $(".loggedout").hide();
          $(".loggedin").show();
          window.location = '#/apis';
	  } else {
		 $(".loginerror").text("Invalid Credentials"); 
	  }
          $(ev.currentTarget).removeClass("loading");
        });
        return false;
      });
    },
    registerHandler: function(){
      $('.registerForm input[type=submit]').live('click', function(){
        var registerDetails = $('.registerForm').serializeObject($('.registerForm'));
        var rerror = $(".registrationerror").html("");
        Session.register(registerDetails, function(data){
          if( data.status === 'ok' ){
            window.location = "#";
          } else { 
            _.each( data.errors, function(error){
              console.log(error);
              switch (error ) {
                case "invalid_user":
                  rerror.append("Username is taken<br />");
                break;
                case "invalid_pass":
                  rerror.append("Password must be at least 4 characters long<br />");
                break;
                case "invalid_email":
                  rerror.append("Email is already in use<br />");
                break;
              }

            });
          } 
        });
        return false;
      });
    }
  });
  return new homeMainView;
});
