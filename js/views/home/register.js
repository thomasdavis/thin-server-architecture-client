define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/home/register.html'
], function($, _, Backbone, registerHomeTemplate){
  var homeRegisterView = Backbone.View.extend({
    
    el: $('#page'),
    render: function(){
      $("#content").html(registerHomeTemplate).hide();
      $("#content").fadeIn(200);

    }

  });
  return new homeRegisterView;
});
