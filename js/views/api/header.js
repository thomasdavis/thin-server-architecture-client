define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/api/header.html'
], function($, _, Backbone, apiHeaderTemplate){
  var apiHeaderView = Backbone.View.extend({
    
    render: function(api, category, method){
      $("#content").prepend( _.template( apiHeaderTemplate, {
        _: _,
        api: api, 
        category: category,
        method: method
      }));
    }

  });
  return new apiHeaderView;
});
