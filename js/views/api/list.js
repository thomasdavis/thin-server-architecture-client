define([
  'jquery',
  'underscore',
  'backbone',
  'collections/apis',
  'text!templates/api/list.html'
], function($, _, Backbone, apiCollection, apiListTemplate){
  var apiListView = Backbone.View.extend({
    el: $("#page"),
    initialize: function(){
      this.apis = new apiCollection;
      var that = this;
      this.apis.bind('reset', function(data){
        that.render();
      });
      this.apis.bind('add', function(data){
        console.log('data', data); 
        $('.create-pane').slideToggle(100);
      });
      this.apis.bind('error', function(err){
        console.log('something else some thing else');
      });
      this.apis.fetch();
      $('.api-list .edit').live('click', function(ev){
        $(this).toggleClass('active');
        $(ev.currentTarget).parents('li').children('.edit-pane').slideToggle(100);
      });
      $('.api-list .delete').live('click', function(ev){
        $(this).toggleClass('active');
        $(ev.currentTarget).parents('li').children('.delete-pane').slideToggle(100);
      });
    },
    events: {
      "click .create": "create",
      "click .create-pane .save": "saveApi"
    },
    create: function(){ 
      $('.create-pane').slideToggle(100);
      console.log("create");
    },
    render: function(){
     
      $("#api").prepend( _.template(apiListTemplate, {apis:this.apis.models, asd: "Asd"}));
    },
    saveApi: function(ev){
      console.log($(ev.currentTarget));
      $(ev.currentTarget).addClass('loading');
      var newCollection = $("#api-create").serializeObject($("#api-create"));
      newCollection = { api: newCollection };
      this.apis.create(newCollection);
      
      return false;
    }

  });
  return new apiListView;
});
