define([
  'jquery',
  'underscore',
  'backbone',
  'views/api/header',
  'views/api/list',
  'views/category/list',
  'views/method/list',
  'views/method/edit',
  'text!templates/api/main.html'
], function($, _, Backbone, apiHeader, apiList, categoryList, methodList, 
methodEdit, apiMainTemplate){
  var apiMainView = Backbone.View.extend({
    el: $("#content"),
    render: function(api, category, method){
      $.ajax({
        url: '/session'
      });	
      $("#content").html(apiMainTemplate).hide();
      $("#content").fadeIn(200);
      apiHeader.render(api, category, method);
      console.log(api, category, method);
      if( method ){
        methodEdit.render();
      } else if ( category ) {
        methodList.render();
      } else if ( api ){   
        categoryList.render();
      } else {
        apiList.render();
      }
        

    }
  });
  return new apiMainView;
});
