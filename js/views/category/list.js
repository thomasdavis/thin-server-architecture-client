define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/category/list.html'
], function($, _, Backbone, categoryListTemplate){
  var apiListView = Backbone.View.extend({
    render: function(){
      $("#api").html(categoryListTemplate);
    }

  });
  return new apiListView;
});
