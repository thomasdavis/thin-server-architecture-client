define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/method/edit.html'
], function($, _, Backbone, methodEditTemplate){
  var apiListView = Backbone.View.extend({
    render: function(){
      $("#api").html(methodEditTemplate);
    }

  });
  return new apiListView;
});
