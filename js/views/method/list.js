define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/method/list.html'
], function($, _, Backbone, methodListTemplate){
  var apiListView = Backbone.View.extend({
    render: function(){
      $("#api").html( methodListTemplate );
    }

  });
  return new apiListView;
});
