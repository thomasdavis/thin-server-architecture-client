define([
  'jquery',
  'underscore',
  'backbone',
  'models/api'
], function($, _, Backbone, apiModel){
  var apiCollection = Backbone.Collection.extend({
    model: apiModel,
    url: '/apis',
    initialize: function(){
      var that = this;
      this.model.bind('error', function(){
        that.trigger('error');
      });
    }
  });
  return apiCollection;
});
