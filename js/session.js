define([
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone){
  var SessionModel = Backbone.Model.extend({
    url: '/session',
    checkAuth: function (options) {
      this.fetch({
        success: function (model) {
          if (model.get('auth')) {
            options.authedCallback();
          } else {
            options.unAuthedCallback();
          }
        }, error: function () {
          console.log('fail',arguments);
        }
      });
    }
  });
  return new SessionModel;
});
