define([
  'jquery', 
  'underscore', 
  'backbone',
  'fakeserver/main',
  'session',
  'router'
], function($, _, Backbone, Fakeserver, Session, Router){
	
  var initialize = function(){
    
    Fakeserver.initialize();
    
    Session.checkAuth({
      authedCallback: function () {
        Router.initialize();
      },
      unAuthedCallback: function () {
        // Go to login screen
      }
    });
  }

  return { 
    initialize: initialize
  };
});
