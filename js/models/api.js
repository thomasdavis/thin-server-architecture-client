define([
  'jquery',
  'underscore',
  'backbone'
], function($, _, Backbone){
  var apiModel = Backbone.Model.extend({
    validate: function(attrs){
      if(typeof attrs.errors !== 'undefined' ){
        return 'Thare errors';
      }  
    }
  });
  return apiModel;
});
