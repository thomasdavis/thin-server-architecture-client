define([
  'sinon',
  'backbone',
  'jquery'
], function(Sinon, Backbone, $){
  var initialize = function(){
    $.ajaxPrefilter( function (options) {
      options.dataType = 'json';
    });
    var that = this;
    this.server = Sinon.fakeServer.create();
    
    this.server.respondWith('GET', '/session',
                            [200, { 'Content-Type': 'application/json' },
                            '{ "auth": true }']);
                                 
    
    var oldSync = Backbone.sync;
    Backbone.sync = function (method, model, options) {
      oldSync( method, model, options );
      that.server.respond();
      that.server.restore();
    }
    
  }

  return { 
    initialize: initialize
  };
});
