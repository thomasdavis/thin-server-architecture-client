define({
  name: 'collaborators',
  methods: [
    {
      name: '/collaborators',
      method: 'POST',
      description: "Add a collaborator to an existing API | Other possible resp: {'error': 'no such api'}, {'error': 'no such user'}, {'auth': false, 'reason': 'not_owner'} ",
      request: {
        collaborator_id: 'id of collab here',
        api_id: 'api id here'
      },
      response: {
        status: 'ok'
      }
    },
    {
      name: '/collaborators',
      method: 'DELETE',
      description: "Remove a collaborator from an existing API | Other possible resp: {'error': 'no such api'}, {'error': 'no such user'}, {'auth': false, 'reason': 'not_owner'} ",
      request: {
        collaborator_id: 'id of collab here',
        api_id: 'api id here'
      },
      response: {
        status: 'ok'
      }
    }
  ]
});
