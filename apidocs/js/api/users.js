define({
  name: 'users',
  methods: [
    { 
      name: '/users/show/:user',
      method: 'GET',
      description: 'Get user info | In case of db fail returns { error: "internal_error" }',
      request: {},
      response: {
        id: 'id_of_the_user_here',
        user: 'username_here'
      }
    },
    { 
      name: '/users/find_by_email/:email',
      method: 'GET',
      description: 'Get user info by specifying his email | In case of db fail returns { error: "internal_error" }',
      request: {},
      response: {
        id: 'id_of_the_user_here',
        user: 'username_here'
      }
    },    
    { 
      name: '/users',
      method: 'POST',
      description: "Create a new user | There are 2 types of errors you can get: a) {type: 'ValidationError', errors: ['invalid_user', 'invalid_email', 'invalid_pass'] b) {type: 'DbError', errors: ['user_or_email_exists']} | In case of db fail returns { error: 'internal_error' }",
      request: {
        user: {
          user: "username_here",
          email: "email_here",
          password: "password_here"
        }
      },
      response: {
        status: "ok"
      }
    },
    { 
      name: '/users/update',
      method: 'PUT',
      description: "Update existing user | There are 2 types of errors you can get: a) {type: 'ValidationError', errors: ['invalid_user', 'invalid_email', 'invalid_pass', 'invalid_new_pass'] b) {type: 'DbError', errors: ['user_or_email_exists', 'unknown']} | In case of db fail returns { error: 'internal_error' } | Note: all the time the user tries to update, he must insert the current pass also",
      request: {
        user: {
          email: "email_here",
          password: "current_password_here",
          new_password: "new_password"
        }
      },
      response: {
        status: "ok"
      }
    }    
  ]


});
