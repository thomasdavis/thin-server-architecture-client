define({
  name: 'sessions',
  methods: [
    {
      name: '/session',
      method: 'GET',
      description: "Check if a user is logged in | If not logged in response is: { auth: false, reason: 'not_auth' }",
      request: {},
      response: {
        auth: true
      }
    },
    { 
      name: '/session',
      method: 'POST',
      description: "Logs a user in | If failed, returns { auth: false, reason: 'invalid_creds'} | In case of db fail returns: {'error': 'internal_error'}",
      request: {
        user: {
          user: "your_username_here",
          password: "your_password_here"
        }
      },
      response: {
        auth: true
      }
    },
    { 
      name: '/session',
      method: 'DELETE',
      description: 'Logs a user out',
      request: {},
      response: {
        status: 'ok',
        csrf: 'new_token'
      }
    }
  ]


});
