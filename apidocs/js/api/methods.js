define({
  name: 'methods',
  methods: [
    {
      name: '/methods',
      method: 'POST',
      description: "Add a method to an api | Other possible resp: { 'error': 'no such api' } OR {'auth': false, 'reason': \"no_access\"}  OR {'type': 'ValidationError', 'errors': ['invalid_title', 'invalid_description', 'invalid_request', 'invalid_method', 'invalid_response']} OR {'type': 'DbError', 'errors': ['unknown']}",
      request: {
        api_id: 'api_id_here',
        api_method: {
          title: 'title of the api method',
          description: 'description of the api method',
          request: 'request JSON obj',
          response: 'response JSON obj',
          method: 'GET or POST or PUT or DELETE',
          'category': 'what category are you asigning this method to'
        }
      },
      response: {
        status: 'ok'
      }
    },
    {
      name: '/methods',
      method: 'PUT',
      description: "Update a method | Other possible resp: { 'error': 'no such api' } OR {'auth': false, 'reason': \"no_access\"}  OR {'type': 'ValidationError', 'errors': ['invalid_title', 'invalid_description', 'invalid_request', 'invalid_method', 'invalid_response']} OR {'type': 'DbError', 'errors': ['unknown']}",
      request: {
        api_method: {
          id: 'id_of_current_api_here',
          title: 'title of the api method',
          description: 'description of the api method',
          request: 'request JSON obj',
          response: 'response JSON obj',
          method: 'GET or POST or PUT or DELETE',
          'category': 'what category are you asigning this method to'
        }
      },
      response: {
        status: 'ok'
      }
    },
    {
      name: '/methods',
      method: 'DELETE',
      description: "Remove a method to an api | Other possible resp:  { 'error': 'no such api' } OR {'auth': false, 'reason': \"no_access\"}",
      request: {
        method_id: 'id of the method here'
      },
      response: {
        status: 'ok'
      }
    }
]});
