define({
  name: 'demo',
  methods: [
    { 
      name: '/demo/create',
      method: 'POST',
      description: 'Logs a user in',
      request: {
        user: "",
        password: ""  
      },
      response: {
        auth: true
      }
    },
    { 
      name: '/demo',
      method: 'DELETE',
      description: 'Logs a user out',
      request: {}
    }
  ]


});
