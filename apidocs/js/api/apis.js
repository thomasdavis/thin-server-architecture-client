define({
  name: 'apis',
  methods: [
    {
      name: '/apis',
      method: 'GET',
      description: "List all the APIs (public or private) of the logged in user",
      request: {},
      response: {
        'name': 'name of the api',
        'owner': 'username_of_the_creator',
        'status': 'public OR private',
        'collaborators':
        [
          {
            "user": "ALessIO3",
            "_id": "4ea30950bcbc20f14d000004"
          },
          {
            "user": "cristina",
            "_id": "4ea79fac96c06e580d000003"
          }
        ],
        'methods': [
          {
              "title": "Some meth2",
              "description": "sss",
              "request": "sss",
              "response": "sss",
              "method": "GET",
              "category": "sports",
              "_id": "4eb057cd600d57441600000f",
              "created": "2011-11-01T20:34:21.915Z"
          },
          {
              "title": "Some meth3",
              "description": "sss",
              "request": "sss",
              "response": "sss",
              "method": "GET",
              "category": "sports",
              "_id": "4eb057d2600d574416000014",
              "created": "2011-11-01T20:34:26.160Z"
          }
        ],
        'created': 'date_here - TODO (whole example)'
      }	
    },
    {
      name: '/apis/by_user/:user',
      method: 'GET',
      description: "List public APIs of the user | Other possible resp: { \"error\": \"no such user\" }",
      request: {},
      response: {
        'name': 'name of the api',
        'owner': 'username_of_the_creator',
        'status': 'public OR private',
        'collaborators':
        [
          {
            "user": "ALessIO3",
            "_id": "4ea30950bcbc20f14d000004"
          },
          {
            "user": "cristina",
            "_id": "4ea79fac96c06e580d000003"
          }
        ],
        'methods': [
          {
              "title": "Some meth2",
              "description": "sss",
              "request": "sss",
              "response": "sss",
              "method": "GET",
              "category": "sports",
              "_id": "4eb057cd600d57441600000f",
              "created": "2011-11-01T20:34:21.915Z"
          },
          {
              "title": "Some meth3",
              "description": "sss",
              "request": "sss",
              "response": "sss",
              "method": "GET",
              "category": "sports",
              "_id": "4eb057d2600d574416000014",
              "created": "2011-11-01T20:34:26.160Z"
          }
        ],
        'created': 'date_here - TODO (whole example)'
      }
    },
    {
      name: '/apis',
      method: 'POST',
      description: "Create a new API | In case of db fail returns { error: \"internal_error\" } | Other possible responses: {'type': 'ValidationError', 'errors': ['invalid_name']} OR {'type': 'DbError', 'errors': ['duplicate_key']} OR {'type': 'DbError', 'errors': ['unknown']}",
      request: {
        api: {
          'name': 'the name of the api',
          'status': 'public OR private',
          'collaborators': ['id1', 'id_user_2', '3rd id']
        }
      },
      response: {
        status: 'ok'
      }	
    },

    {
      name: '/apis',
      method: 'PUT',
      description: "Update existing API | Other possible resp: {\"error\": \"no such api\"} OR { \"auth\": false, \"reason\": \"no_access\" } (this means you don't have access to edit that api) OR same errors from creating an Api",
      request: {
        api: {
          'id': 'id of the api you are updating',
          'name': 'the name of the api',
          'status': 'public OR private',
          'collaborators': ['id1', 'id_user_2', '3rd id']
        }
      },
      response: {
        status: 'ok'
      }
    },
    {
      name: '/apis',
      method: 'DELETE',
      description: 'Delete existing API | Other possible resp: {"error": "no such api"}, {"error": "not_owner"}',
      request: {
        'id': 'id of the api'
      },
      response: {
        status: 'ok'
      }
    }

  ]

});
