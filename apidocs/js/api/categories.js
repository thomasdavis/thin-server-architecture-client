define({
  name: 'categories',
  methods: [
    {
      name: '/categories',
      method: 'PUT',
      description: 'Update a category name | Other possible responses: {"error": "no such api"}, {"error": "no such category"}, {"error": "bad category name"} <- this refers to the new name, {"auth": false, "reason": "no_access"} | Note: if user enters "" (blank) as the category to be updated, let him do that cuz that means he will change all the methods that are assigned to no category to be assigned to the "new_category" he enters.',
      request: {
        category: 'Name of the category you want to update',
        new_category: 'New name of the category',
        api_id: 'Id of the api that contains the category'
      },
      response: {
        status: 'ok'
      }
    },
    {
      name: '/categories',
      method: 'DELETE',
      description: 'Delete a category | Other possible responses: {"error": "no such api"}, {"error": "no such category"}, {"auth": false, "reason": "no_access"}',
      request: {
        category: 'Name of the category you want to delete',
        api_id: 'Id of the api that contains the category'
      },
      response: {
        status: 'ok'
      }
    }
  ]
});
