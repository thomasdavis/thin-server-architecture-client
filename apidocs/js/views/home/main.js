define([
  'jQuery',
  'Underscore',
  'Backbone',
  'text!templates/home/main.html',
  'text!templates/home/api.html',
  'api/session',
  'api/users',
  'api/apis',
  'api/methods',
  'api/collaborators',
  'api/categories'
], function($, _, Backbone, mainHomeTemplate, apiTemplate,
  sessionAPI,
  usersAPI,
  apisAPI,
  methodsAPI,
  collaboratorsAPI,
  categoriesAPI
){

  var mainHomeView = Backbone.View.extend({
    el: $('#page'),
    render: function(){
      this.el.html(mainHomeTemplate);
      this.el.append( _.template( apiTemplate, sessionAPI) );
      this.el.append( _.template( apiTemplate, usersAPI) );
      this.el.append( _.template( apiTemplate, apisAPI) );
      this.el.append( _.template( apiTemplate, methodsAPI) );
      this.el.append( _.template( apiTemplate, collaboratorsAPI) );
      this.el.append( _.template( apiTemplate, categoriesAPI) );
    },
    events: {
      'click .methods li': 'showMore'
    },
    showMore: function(ev){ 
      $(ev.currentTarget).children('.more').toggle();
    }
  });
  return new mainHomeView;
});
